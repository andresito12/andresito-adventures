

// create a new scene named "Game"
let gameScene = new Phaser.Scene('Game');

  var keyA;
  var keyD;
  var shapeArray;
  var food;
  var numFruits;
  var lastFruit;
  var player;
  
// load asset files for our game
gameScene.preload = function() {
  this.load.image('pizza', 'assets/pizza.png');
  this.load.image('strawberry', 'assets/strawberry.png');
  this.load.image('burger', 'assets/burger.png');
  this.load.image('brocoli', 'assets/brocoli.png');
  this.load.image('background', 'assets/playa2s.png');
  this.load.image('player', 'assets/andres.png');
  this.load.image('Red Bull', 'assets/pixil-frame-0 (1).png');
}; 
// executed once, after assets were loaded
gameScene.create = function() {
  // background 
  this.bg = this.add.sprite(0, 0, 'background');
  this.bg.setScale(0.3);
  player = this.add.sprite(320, 350, 'player');
  this.playerSpeed = 1.5; 
  numFruits = 5
  //Remember that sprites are createn on top of each other 
  
  keyA = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A);
  keyD = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D);
  // shapeArray = [
  //   pizzaSpr = this.add.sprite(this.getRandomInt(641), 10, 'pizza'),
  //   burgerSpr = this.add.sprite(this.getRandomInt(641), 10, 'burger'),
  //   brocoliSpr = this.add.sprite(this.getRandomInt(641), 10, 'brocoli'),
  //   stawSpr = this.add.sprite(this.getRandomInt(641), 10, 'strawberry'),
  //   redBullSpr = this.add.sprite(this.getRandomInt(641), 10, 'Red Bull')
  // ];
  
      group = this.add.group({
        maxSize: 5,
        createCallback: function (fruits) {
          fruits.setName('fruit' + this.getLength());
          console.log('Created', fruits.name);
        },
        removeCallback: function (fruits) {
          console.log('Removed', fruits.name);
      }
    });

      group.create(this.getRandomInt(641), 10, 'pizza', 0, 1);
      group.create(this.getRandomInt(641), 10, 'burger', 0, 1);
      group.create(this.getRandomInt(641), 10, 'brocoli', 0, 1);
      group.create(this.getRandomInt(641), 10, 'redbull', 0, 1);
      group.create(this.getRandomInt(641), 10, 'strawberry', 0, 1);

      Phaser.Actions.ScaleXY(group.getChildren(), -0.8, -0.8);

      this.time.addEvent({
        delay: 2000,
        loop: true,
        callback: gameScene.addFruit()
    });


    };

gameScene.getRandomInt = function(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

// executed on every frame (60 times per second)
gameScene.update = function() {
  
    // for(i = 0; i < numFruits; i++) {
    //   group.getChildren[i].setVisible(false);
    //   if (group.getChildren[i].y < 360){
    //     group.getChildren[i].setVisible(true);
    //     return;
    //   }
    //   if (group.getChildren[i].y >= 360){
    //     group.getChildren[i].setVisible(false);
    //     return;
    //  }
    // }

  Phaser.Actions.IncY(group.getChildren(), 1);

  group.children.iterate(function (fruits) {
    console.log("Iterate");
    
    // enemy collision
  if (Phaser.Geom.Intersects.RectangleToRectangle(player.getBounds(), fruits.getBounds())) {
    console.log("Collision::::::>"+JSON.stringify(fruits));
      console.log("Nombre: "+fruits.name);
  }

                    if (fruits.y > 380) {
                        group.killAndHide(fruits);
                        fruits.setVisible(false);
                        console.log("Set False");
                        fruits.y = 0;
                        
                    }
                    else if(fruits.y < 380){
                      fruits.setVisible(true);
                      console.log("Set true");
                    }
                  })
  
  if(keyD.isDown){
    player.x += this.playerSpeed;
  }
  if(keyA.isDown){
    player.x -= this.playerSpeed;
  }
};
  
gameScene.addFruit = function () {
  console.log("Add fruit Function");
  var fruit = group.get(Phaser.Math.Between(0, 640), Phaser.Math.Between(-64, 0));
  console.log("Fruit: "+JSON.stringify(fruit));
  if (!fruit) return; // None free

  activateFruit(fruits);
}

function activateFruit (fruits ){
  console.log("Activate fruit Function");
  fruits
  .setActive(true)
  .setVisible(true);
  console.log("activateFruit");
}

// our game's configuration
let config = {
  type: Phaser.AUTO,
  width: 640,
  height: 360,
  scene: gameScene
}

// create the game, and pass it the configuration
let game = new Phaser.Game(config);

